# Eagle PCB Projects

## Some examples:

* LoRa to WiFi Gateway Hub
![Alt text](/GatewayHub/GatewayHub-pcb_v2.2.png?raw=true "LoRa to WiFi Gateway")

* LoRa sensor
![Alt text](/oshpark_pcb/RF_Sensor_v1.0/_pcb_top.png?raw=true "LoRa Sensor")
 
* DCC Driver for my HO train layout
![Alt text](/oshpark_pcb/dcc2_example/large_i.png?raw=true "DCC Driver HO Train")

* PWM Driver for my HO train layout
![Alt text](/PWM-cab/PWM-cab_pcb-view.jpg?raw=true "PWM Cab driver")

